declare module "@salesforce/schema/Opportunity.Id" {
  const Id:any;
  export default Id;
}
declare module "@salesforce/schema/Opportunity.IsDeleted" {
  const IsDeleted:boolean;
  export default IsDeleted;
}
declare module "@salesforce/schema/Opportunity.Account" {
  const Account:any;
  export default Account;
}
declare module "@salesforce/schema/Opportunity.AccountId" {
  const AccountId:any;
  export default AccountId;
}
declare module "@salesforce/schema/Opportunity.RecordType" {
  const RecordType:any;
  export default RecordType;
}
declare module "@salesforce/schema/Opportunity.RecordTypeId" {
  const RecordTypeId:any;
  export default RecordTypeId;
}
declare module "@salesforce/schema/Opportunity.Name" {
  const Name:string;
  export default Name;
}
declare module "@salesforce/schema/Opportunity.Description" {
  const Description:string;
  export default Description;
}
declare module "@salesforce/schema/Opportunity.StageName" {
  const StageName:string;
  export default StageName;
}
declare module "@salesforce/schema/Opportunity.Amount" {
  const Amount:number;
  export default Amount;
}
declare module "@salesforce/schema/Opportunity.Probability" {
  const Probability:number;
  export default Probability;
}
declare module "@salesforce/schema/Opportunity.CloseDate" {
  const CloseDate:any;
  export default CloseDate;
}
declare module "@salesforce/schema/Opportunity.Type" {
  const Type:string;
  export default Type;
}
declare module "@salesforce/schema/Opportunity.NextStep" {
  const NextStep:string;
  export default NextStep;
}
declare module "@salesforce/schema/Opportunity.LeadSource" {
  const LeadSource:string;
  export default LeadSource;
}
declare module "@salesforce/schema/Opportunity.IsClosed" {
  const IsClosed:boolean;
  export default IsClosed;
}
declare module "@salesforce/schema/Opportunity.IsWon" {
  const IsWon:boolean;
  export default IsWon;
}
declare module "@salesforce/schema/Opportunity.ForecastCategory" {
  const ForecastCategory:string;
  export default ForecastCategory;
}
declare module "@salesforce/schema/Opportunity.ForecastCategoryName" {
  const ForecastCategoryName:string;
  export default ForecastCategoryName;
}
declare module "@salesforce/schema/Opportunity.Campaign" {
  const Campaign:any;
  export default Campaign;
}
declare module "@salesforce/schema/Opportunity.CampaignId" {
  const CampaignId:any;
  export default CampaignId;
}
declare module "@salesforce/schema/Opportunity.HasOpportunityLineItem" {
  const HasOpportunityLineItem:boolean;
  export default HasOpportunityLineItem;
}
declare module "@salesforce/schema/Opportunity.Pricebook2" {
  const Pricebook2:any;
  export default Pricebook2;
}
declare module "@salesforce/schema/Opportunity.Pricebook2Id" {
  const Pricebook2Id:any;
  export default Pricebook2Id;
}
declare module "@salesforce/schema/Opportunity.Owner" {
  const Owner:any;
  export default Owner;
}
declare module "@salesforce/schema/Opportunity.OwnerId" {
  const OwnerId:any;
  export default OwnerId;
}
declare module "@salesforce/schema/Opportunity.CreatedDate" {
  const CreatedDate:any;
  export default CreatedDate;
}
declare module "@salesforce/schema/Opportunity.CreatedBy" {
  const CreatedBy:any;
  export default CreatedBy;
}
declare module "@salesforce/schema/Opportunity.CreatedById" {
  const CreatedById:any;
  export default CreatedById;
}
declare module "@salesforce/schema/Opportunity.LastModifiedDate" {
  const LastModifiedDate:any;
  export default LastModifiedDate;
}
declare module "@salesforce/schema/Opportunity.LastModifiedBy" {
  const LastModifiedBy:any;
  export default LastModifiedBy;
}
declare module "@salesforce/schema/Opportunity.LastModifiedById" {
  const LastModifiedById:any;
  export default LastModifiedById;
}
declare module "@salesforce/schema/Opportunity.SystemModstamp" {
  const SystemModstamp:any;
  export default SystemModstamp;
}
declare module "@salesforce/schema/Opportunity.LastActivityDate" {
  const LastActivityDate:any;
  export default LastActivityDate;
}
declare module "@salesforce/schema/Opportunity.FiscalQuarter" {
  const FiscalQuarter:number;
  export default FiscalQuarter;
}
declare module "@salesforce/schema/Opportunity.FiscalYear" {
  const FiscalYear:number;
  export default FiscalYear;
}
declare module "@salesforce/schema/Opportunity.Fiscal" {
  const Fiscal:string;
  export default Fiscal;
}
declare module "@salesforce/schema/Opportunity.Contact" {
  const Contact:any;
  export default Contact;
}
declare module "@salesforce/schema/Opportunity.ContactId" {
  const ContactId:any;
  export default ContactId;
}
declare module "@salesforce/schema/Opportunity.LastViewedDate" {
  const LastViewedDate:any;
  export default LastViewedDate;
}
declare module "@salesforce/schema/Opportunity.LastReferencedDate" {
  const LastReferencedDate:any;
  export default LastReferencedDate;
}
declare module "@salesforce/schema/Opportunity.SyncedQuote" {
  const SyncedQuote:any;
  export default SyncedQuote;
}
declare module "@salesforce/schema/Opportunity.SyncedQuoteId" {
  const SyncedQuoteId:any;
  export default SyncedQuoteId;
}
declare module "@salesforce/schema/Opportunity.Contract" {
  const Contract:any;
  export default Contract;
}
declare module "@salesforce/schema/Opportunity.ContractId" {
  const ContractId:any;
  export default ContractId;
}
declare module "@salesforce/schema/Opportunity.HasOpenActivity" {
  const HasOpenActivity:boolean;
  export default HasOpenActivity;
}
declare module "@salesforce/schema/Opportunity.HasOverdueTask" {
  const HasOverdueTask:boolean;
  export default HasOverdueTask;
}
declare module "@salesforce/schema/Opportunity.Budget_Confirmed__c" {
  const Budget_Confirmed__c:boolean;
  export default Budget_Confirmed__c;
}
declare module "@salesforce/schema/Opportunity.Discovery_Completed__c" {
  const Discovery_Completed__c:boolean;
  export default Discovery_Completed__c;
}
declare module "@salesforce/schema/Opportunity.ROI_Analysis_Completed__c" {
  const ROI_Analysis_Completed__c:boolean;
  export default ROI_Analysis_Completed__c;
}
declare module "@salesforce/schema/Opportunity.Loss_Reason__c" {
  const Loss_Reason__c:string;
  export default Loss_Reason__c;
}
declare module "@salesforce/schema/Opportunity.telefone__c" {
  const telefone__c:number;
  export default telefone__c;
}
declare module "@salesforce/schema/Opportunity.email__c" {
  const email__c:string;
  export default email__c;
}
declare module "@salesforce/schema/Opportunity.produtosInteresse__c" {
  const produtosInteresse__c:string;
  export default produtosInteresse__c;
}
declare module "@salesforce/schema/Opportunity.classificacao__c" {
  const classificacao__c:string;
  export default classificacao__c;
}
declare module "@salesforce/schema/Opportunity.motivoPerda__c" {
  const motivoPerda__c:string;
  export default motivoPerda__c;
}
declare module "@salesforce/schema/Opportunity.numeroConversoes__c" {
  const numeroConversoes__c:number;
  export default numeroConversoes__c;
}
declare module "@salesforce/schema/Opportunity.origemPrimeiraConversao__c" {
  const origemPrimeiraConversao__c:string;
  export default origemPrimeiraConversao__c;
}
declare module "@salesforce/schema/Opportunity.mesesCriacaoOportunidade__c" {
  const mesesCriacaoOportunidade__c:number;
  export default mesesCriacaoOportunidade__c;
}
declare module "@salesforce/schema/Opportunity.Vencimento_de_oportunidade__c" {
  const Vencimento_de_oportunidade__c:number;
  export default Vencimento_de_oportunidade__c;
}
declare module "@salesforce/schema/Opportunity.Controle_stage_fases__c" {
  const Controle_stage_fases__c:boolean;
  export default Controle_stage_fases__c;
}
declare module "@salesforce/schema/Opportunity.Sku__c" {
  const Sku__c:string;
  export default Sku__c;
}
declare module "@salesforce/schema/Opportunity.Numero_Visitas_CheckOut__c" {
  const Numero_Visitas_CheckOut__c:number;
  export default Numero_Visitas_CheckOut__c;
}
declare module "@salesforce/schema/Opportunity.Criado_Mkt_Cld__c" {
  const Criado_Mkt_Cld__c:boolean;
  export default Criado_Mkt_Cld__c;
}
declare module "@salesforce/schema/Opportunity.Historico_de_visitas__c" {
  const Historico_de_visitas__c:string;
  export default Historico_de_visitas__c;
}
declare module "@salesforce/schema/Opportunity.dataUltimoCheckout__c" {
  const dataUltimoCheckout__c:any;
  export default dataUltimoCheckout__c;
}
