/*******************************************************************************
*                               Werise - 2020
*-------------------------------------------------------------------------------
*
*	Test class for TestDataFactory
*
* NAME: TestDataFactory_test
* AUTHOR: Jefferson F. Presotto                                DATE: 08/01/2020
* UPDATED: João Vitor Ramos                                    DATE: 16/10/2020
*******************************************************************************/

@isTest
public class TestDataFactory_test {
    
    /*@isTest
    public static void testCreate_Accounts_Contact(){

        User usr = TestDataFactory.getUser();
        
        List<Account> listAcc = TestDataFactory.createAccounts(1);
        TestDataFactory.createContacts(listAcc, 1);
        TestDataFactory.createLeads(1, usr.Id);
        
        test.startTest();
        List<Account> accList = [SELECT id, Name FROM Account LIMIT 1];
        List<Contact> conList = [SELECT id, FirstName FROM Contact LIMIT 1];
        test.stopTest();
        
        System.assertEquals(accList[0].Name, 'TestAccount n:0');
        System.assertEquals(conList[0].FirstName, 'Test-FirstName0');
    }
    
    @isTest
    public static void testCreate_Opportunity_Campaign(){
        
        List<Account> listAcc = TestDataFactory.createAccounts(1);
        List<Contact> conList = TestDataFactory.createContacts(listAcc, 1);
        List<Campaign> campList = TestDataFactory.createCampaigns(1);
        List<Opportunity> oppList = TestDataFactory.createOpps(conList, System.today() +10, campList[0]);
        
        test.startTest();
        List<Campaign> listCamp = [SELECT id, Name FROM Campaign LIMIT 1];
        List<Opportunity> listOpp = [SELECT id, Name FROM Opportunity LIMIT 1];
        test.stopTest();
        
        System.assertEquals(listCamp[0].Name, 'Campaign Test');
        System.assertEquals(listOpp[0].Name, 'Opp Teste -- Test-FirstName0');
        
    }*/
    
    @isTest
    public static void testCreate_Product_LineItem_PriceBook(){
        
        List<String> prodNameList = new List<String>{'50', '51', '52'};
        List<Product2> prodList = TestDataFactory.createProduct(prodNameList);
        List<PricebookEntry> stdPriceBook = TestDataFactory.createPricebookEntry(prodList);
        
        test.startTest();
        List<PricebookEntry> priceBookList = [SELECT id, UnitPrice FROM PricebookEntry LIMIT 1];
        List<Product2> listProd = [SELECT id, Name FROM Product2 LIMIT 1];
        test.stopTest();
        
        System.assertEquals(priceBookList[0].UnitPrice, 300);
        System.assertEquals(prodList[0].Name, 'Product Test (50)');
        
    }
}