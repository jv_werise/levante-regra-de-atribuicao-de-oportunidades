/*******************************************************************************
*                               Werise - 2020
*-------------------------------------------------------------------------------
*
*	Class to process the leads of abandoned cart event
*
* START PROCESS: LeadTrigger
* NAME: ShoppingCartProcess
* AUTHOR: Jefferson F. Presotto                                DATE: 07/01/2020
* UPDATED: João Vitor Ramos                                    DATE: 16/10/2020
*******************************************************************************/

global class ShoppingCartProcess implements Database.Batchable<sObject>{
    
    global final Set<Id> setLeadId;
    
    global ShoppingCartProcess (Set<Id> leads){
        this.setLeadId = leads;
    }
    
    global Database.QueryLocator start(Database.BatchableContext BC){
        return Database.getQueryLocator('SELECT Id, Name, Email, Criar_Oportunidade__c, ID_Produto__c, Numero_Visitas_CheckOut__c, Historico_de_visitas__c ' + 
                                        'FROM Lead ' + 
                                        'WHERE Id IN: setLeadId');
    }
    
    global void execute(Database.BatchableContext BC, List<Lead> scope){
        LeadConvert.LeadAssign(scope);
    }
    
    public static void createOpportunity(Set<String> setLeadId, Map<String, String> map_leadId_accountId_future){
        List<Lead> leadList = LeadDAO.getLeadById(setLeadId);
        List<Opportunity> opportunityList = new List<Opportunity>();
        Opportunity opp;
        
        List<RecordType> typeId = [SELECT id FROM RecordType  WHERE name like 'abandoned%' LIMIT 1];
        
        //for each account create the opportunity
        for(Lead ld : leadList){
            opp = new Opportunity(Name = ld.Email + '-' + ld.ID_Produto__c,
                                  sku__c = ld.ID_Produto__c,
                                  Numero_Visitas_CheckOut__c = ld.Numero_Visitas_CheckOut__c,
                                  accountId = map_leadId_accountId_future.get(ld.Id),
                                  stageName = 'Classificação',
                                  CloseDate = System.today() + 7,
                                  email__c = ld.Email,
                                  RecordTypeId = typeId[0].id,
                                  Criado_Mkt_Cld__c = true,
                                  Historico_de_visitas__c = ld.Historico_de_visitas__c);                                  
            opportunityList.add(opp);
        }
        insert opportunityList;
        //createProduct(opportunityList);
    }
    
    //create the product
    public static void createProduct(List<Opportunity> opportunityList){
        List<OpportunityLineItem> oppLineItemList = new List<OpportunityLineItem>();
        Map<String, PriceBookEntry> map_productCode_PriceBookEntry = new Map<String, PriceBookEntry>();
        OpportunityLineItem oli;
        
        List<PriceBookEntry> priceBookList = PriceBookEntryDAO.getPriceBooks();
        for(PriceBookEntry pbe : priceBookList){
            map_productCode_PriceBookEntry.put(pbe.Product2.ProductCode, pbe);
        }
        for(Opportunity opp : opportunityList){
            oli = new OpportunityLineItem(
                OpportunityId = opp.Id,
                Quantity = 1,
                numeroVisitasCheckOut__c = opp.Numero_Visitas_CheckOut__c);
            if(map_productCode_PriceBookEntry.get(opp.Sku__c) != null){
                oli.UnitPrice = map_productCode_PriceBookEntry.get(opp.Sku__c).UnitPrice;
                oli.PricebookEntryId = map_productCode_PriceBookEntry.get(opp.Sku__c).Id;
                oppLineItemList.add(oli);
            }
        }
        insert oppLineItemList;
    }
    global void finish(Database.BatchableContext BC){
    }
}