/*******************************************************************************
*                               Werise - 2020
*-------------------------------------------------------------------------------
*
*	Class reponsible for distibute the accounts to specific SDRs
*
*   Test class : OpportunityTrigger_test
*   Called from : OpportunityTrigger
*
* NAME: AccountQueueDistributionBatch
* AUTHOR: João Vitor Ramos                                      DATE: 16/10/2020
*******************************************************************************/
public class AccountQueueDistributionBatch {

    public static void changeAccountOwner(Set<Id> idQueues, List<Account> accountList){
        System.debug('%%%%%%%%%%%%%%%% ACCOUNTQUEUEDISTRIBUTIONBATCH UPDATING THE ACCOUNTS OWNERS %%%%%%%%%%%%%%%%');

        //getting the werise user to filter the accounts and the groups to distribute
        User usuario = [select Id, alias from User where alias = 'werise'];
        List<Group> grupos = [SELECT Id, Name, type FROM Group WHERE Id IN :idQueues];
        List<Account> accountFilter = [SELECT Id FROM Account
                                        WHERE Id IN :accountList AND OwnerId = :usuario.Id];

        //for each queue
        for(Group grupo : grupos){
            List<Account> accountOppBasedList = new List<Account>();
            if(accountFilter.isEmpty()){
                system.debug('%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% DEBUG SEM CONTA %%%%%%%%%%%%%');
                continue;
            }
            //filtering the accounts based on opportunity amount and opportunity created date for each group. 
            if(grupo.name == 'GrupoJunior'){
                accountOppBasedList = [SELECT Id, OwnerId, CreatedDate
                                FROM Account
                                WHERE Id IN :accountFilter AND Id IN (SELECT AccountId FROM Opportunity WHERE Amount <= 500.00 AND CreatedDate = TODAY)];
            }
            else if(grupo.name == 'GrupoPleno'){
                accountOppBasedList = [SELECT Id, OwnerId, CreatedDate
                                FROM Account
                                WHERE Id IN :accountFilter AND Id IN (SELECT AccountId FROM Opportunity WHERE Amount > 500.00 AND Amount <= 10000.00 AND CreatedDate = TODAY)];
            }
            else if (grupo.name == 'GrupoPlenoInfinite'){
                accountOppBasedList = [SELECT Id, OwnerId, CreatedDate
                                FROM Account
                                WHERE Id IN :accountFilter AND Id IN (SELECT AccountId FROM Opportunity WHERE Amount > 10000.00 AND CreatedDate = TODAY)];
            }

            if(accountOppBasedList.isEmpty()) continue;

            //declaring the variables used in the account distribution logic
            GrupoSDRs__c lastSdr;
            List<User> userList = new List<User>();
            List<GrupoSDRs__c> listSdr = new List<GrupoSDRs__c>();
            List<GrupoSDRs__c> listFilterSdr = new List<GrupoSDRs__c>();
            List<User> lastSdrList = new List<User>();
            Set<Id> setSdrId = new Set<Id>();
            Map<Id, GrupoSDRs__c> mapSdrIdBySrd = new Map<Id, GrupoSDRs__c>();
            //query all the sdrs
            listSdr = GrupoSDRs__c.getall().values();

            //get only the sdrs for the specific queue
            for(GrupoSDRs__c sdr : listSdr){
                if(sdr.Id_Fila__c == grupo.id) listFilterSdr.add(sdr);
                system.debug('%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% DEBUG SDR %%%%%%%%%%%%%');
            }
            if(listFilterSdr.isEmpty()) continue;
            //get sdr data and put into a set and map
            for(GrupoSDRs__c sdr : listFilterSdr){
                setSdrId.add(sdr.Id_Usuario__c);
                mapSdrIdBySrd.put(sdr.Id_Usuario__c, sdr);
                //get last sdr that received account
                if(sdr.Recebeu_Conta__c == true) lastSdr = sdr;
            }

            //query the users for the sdrs
            userList = [SELECT Id, Name FROM User WHERE Id IN: setSdrId ORDER BY Name ASC];
            if(lastSdr != null){
                //first step to set the list order
                for(User us : userList){
                    lastSdrList.add(us);
                    if(us.Id == lastSdr.Id_Usuario__c) lastSdrList.clear();
                }
                //second step to set the list order
                for(User us : userList){
                    if(lastSdrList.contains(us)){
                        continue;
                    } else {
                        lastSdrList.add(us);
                    }
                }
                //update the last sdr
                lastSdr.Recebeu_Conta__c = false;
                update lastSdr;
                
            } else {
                lastSdrList = userList;
            }
            //counters
            Integer accountCounter = 0;
            Integer sdrCounter = 0;
            List<GrupoSDRs__c> sdrObjectCounter = new List<GrupoSDRs__c>();
            List<GrupoSDRs__c> sdrToUpdate = new List<GrupoSDRs__c>();
            List<Account> accToUpdate = new List<Account>();

            for(Account acc : accountOppBasedList){
                system.debug('%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% DEBUG CONTA %%%%%%%%%%%%%');
                accountCounter++;
                for(User usr : lastSdrList){
                    if(!sdrObjectCounter.contains(mapSdrIdBySrd.get(usr.Id))){
                    sdrObjectCounter.add(mapSdrIdBySrd.get(usr.Id));
                    sdrCounter++;
                    //update the account owner
                    acc.OwnerId = usr.Id;
                    accToUpdate.add(acc);
                    //put the sdr in the list to update if is the last account
                    sdrToUpdate.clear();
                    GrupoSDRs__c sdr = mapSdrIdBySrd.get(usr.Id);
                    sdr.Recebeu_Conta__c = true;
                    sdrToUpdate.add(sdr);
                    break;
                    } 
                    else {
                        if(userList.size() == sdrCounter){
                            sdrObjectCounter.clear();
                            sdrCounter = 0;
                            //update the account owner
                            acc.OwnerId = lastSdrList[0].Id;
                            accToUpdate.add(acc);
                            sdrObjectCounter.add(mapSdrIdBySrd.get(lastSdrList[0].Id));
                            sdrCounter++;
                            if(accountOppBasedList.size() == accountCounter){
                                //put the sdr in the list to update if is the last account
                                sdrToUpdate.clear();
                                GrupoSDRs__c sdr = mapSdrIdBySrd.get(lastSdrList[0].Id);
                                sdr.Recebeu_Conta__c = true;
                                sdrToUpdate.add(sdr);
                            }
                            break;
                        } 
                        else {
                            continue;
                        }
                    }
                }
            }
            update accToUpdate;
            update sdrToUpdate;
        }
    }
}