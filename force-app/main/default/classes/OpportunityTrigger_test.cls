/*******************************************************************************
*                               Werise - 2020
*-------------------------------------------------------------------------------
*
*	Class to test the opportunity trigger, 
*
* NAME: OpportunityTrigger_test
* AUTHOR: Jefferson F. Presotto                                DATE: 18/02/2020
* UPDATED: João Vitor Ramos                                    DATE: 16/10/2020
*******************************************************************************/
@isTest
public class OpportunityTrigger_test {
    
    @testSetup 
    static void setup(){
        List<User> usr = [SELECT Id, Name FROM User WHERE Email != 'projetos@werise.com.br' AND usertype = 'Standard' AND isActive = true];
        List<Group> queues = new List<Group>();
        List<groupMember> members = new List<GroupMember>();
        List<GrupoSDRs__c> sdrs = new List<GrupoSDRs__c>();

        //creating the public groups and populating with group members
        Group queue1 = new Group (Name = 'GrupoJunior', Type = 'Regular');
        queues.add(queue1);
        Group queue2 = new Group (Name = 'GrupoPleno', Type = 'Regular');
        queues.add(queue2);
        Group queue3 = new Group (Name = 'GrupoPlenoInfinite', Type = 'Regular');
        queues.add(queue3);
        insert queues;

        GroupMember gpMember11 = new GroupMember (GroupId = queue1.Id, UserOrGroupId = usr[0].Id);
        members.add(gpMember11);
        GroupMember gpMember12 = new GroupMember (GroupId = queue1.Id, UserOrGroupId = usr[1].Id);
        members.add(gpMember12);
        GroupMember gpMember21 = new GroupMember (GroupId = queue2.Id, UserOrGroupId = usr[2].Id);
        members.add(gpMember21);
        GroupMember gpMember31 = new GroupMember (GroupId = queue3.Id, UserOrGroupId = usr[3].Id);
        members.add(gpMember31);
        insert members;

        System.runAs(new User(Id = UserInfo.getUserId())){        
            //creating the sdr records of the users in the queues
            Integer counter = 0;                       
            for(GroupMember gm : members){
                counter++;
                GrupoSDRs__c sdr = new GrupoSDRs__c(Id_Usuario__c = gm.UserOrGroupId,
                                            Recebeu_Conta__c = counter==1 ? true:false,
                                            Id_Fila__c = gm.GroupId,
                                            Name = 'test'+ counter + ' - ' + String.valueOf(gm.GroupId).substring(11));
                sdrs.add(sdr);
            }
            insert sdrs;
        }
    }

    @isTest    
    public static void testInsertTrigger() {
        //creating products and pricebookentries
        List<String> listProdCode = new List<String> {'50', '51', '52'};
        List<Product2> prods = TestDataFactory.createProduct(listProdCode);
        List<PricebookEntry> entries = TestDataFactory.createPricebookEntry(prods);

        //creating lists to handle the logic
        Id typeId = [SELECT id FROM RecordType  WHERE name like 'abandoned%' LIMIT 1][0].Id;        
        User user = [SELECT Id FROM User WHERE Name = 'Werise'];
        List<Account> accList = new List<Account>();
        List<Opportunity> oppList = new List<Opportunity>();
        Integer counter = 0;

        for(Integer i = 0; i<30;i++){
            Account acc = new Account(Name = 'TestAccOpp'+i, OwnerId = user.Id);
            acclist.add(acc);
        }
        insert acclist;

        for(Account acc : accList){
            Opportunity opp = new Opportunity();
            opp.AccountId = acc.Id;
            opp.Name = acc.Id + ' - ' + String.valueOf(50+counter);
            opp.CloseDate = Date.today().addDays(7);
            opp.StageName = 'Classificação';
            opp.Sku__c = String.valueOf(50+counter);
            opp.Criado_Mkt_Cld__c = true;
            opp.RecordTypeId = typeId;
            opp.Numero_Visitas_CheckOut__c = 10;
            opp.email__c = acc.Name + '@teste.com';

            counter++;
            if(counter == 3)
                counter = 0;

            opplist.add(opp);
        }
        
        Test.startTest();
        Insert opplist;
        Test.stopTest();

        List<Account> updatedAccount = [select id from account where ownerId != :user.Id];
        System.assertEquals(30, updatedAccount.size());
    }
    
    static testmethod void testUpdateTrigger() {
        List<RecordType> typeId = [SELECT id FROM RecordType  WHERE name like 'abandoned%' LIMIT 1];
        
        User user = [SELECT Id FROM User WHERE Name = 'Werise'];
        
        Account account = new Account(Name = 'test');
        insert account;
        
        Opportunity opp = new Opportunity(Name = 'Test',
                                          //sku__c = '1',
                                          OwnerId = user.Id,
                                          Numero_Visitas_CheckOut__c = 10,
                                          stageName = 'Classificação',
                                          CloseDate = Date.today().addDays(7),
                                          email__c = 'teste@teste.com',
                                          AccountId = account.Id,
                                          RecordTypeId = typeId[0].id);
        insert opp;
        
        opp.Criado_Mkt_Cld__c = true;
        opp.Numero_Visitas_CheckOut__c = 20;
        
        Test.startTest();
        update opp;
        Test.stopTest();
    }
    
    @isTest
    public static void testFechadoEGanho() {
        List<RecordType> typeId = [SELECT id FROM RecordType  WHERE name like 'abandoned%' LIMIT 1];
        
        User user = [SELECT Id FROM User WHERE Name = 'Werise'];
        
        Account account = new Account(Name = 'test');
        insert account;
        
        Opportunity opp = new Opportunity(Name = 'Test',
                                          OwnerId = user.Id,
                                          Numero_Visitas_CheckOut__c = 10,
                                          stageName = 'Classificação',
                                          CloseDate = Date.today().addDays(7),
                                          email__c = 'teste@teste.com',
                                          RecordTypeId = typeId[0].id,
                                          AccountId = account.Id,
                                          Criado_Mkt_Cld__c = true);
        insert opp;
        
        opp.StageName = 'Fechado e Ganho';
        Test.startTest();
        update opp;
        Test.stopTest();
    }
    @isTest
    public static void testFechadoEPerdido() {
        List<RecordType> typeId = [SELECT id FROM RecordType  WHERE name like 'abandoned%' LIMIT 1];
        
        User user = [SELECT Id FROM User WHERE Name = 'Werise'];
        
        Account account = new Account(Name = 'test');
        insert account;
        Opportunity opp = new Opportunity(Name = 'Test',
                                          OwnerId = user.Id,
                                          Numero_Visitas_CheckOut__c = 10,
                                          stageName = 'Classificação',
                                          CloseDate = Date.today().addDays(7),
                                          email__c = 'teste@teste.com',
                                          RecordTypeId = typeId[0].id,
                                          AccountId = account.Id,
                                          Criado_Mkt_Cld__c = true);
        insert opp;
        
        opp.StageName = 'Fechado e Perdido';
        opp.Numero_Visitas_CheckOut__c = 15;

        Test.startTest();
        update opp;
        Test.stopTest();
    }
}