/*******************************************************************************
*                               Werise - 2020
*-------------------------------------------------------------------------------
*
*	Class for create test data in tests class
*
* NAME: TestDataFactory
* AUTHOR: Jefferson F. Presotto                                DATE: 07/01/2020
* UPDATED: João Vitor Ramos                                    DATE: 16/10/2020
*******************************************************************************/

public class TestDataFactory {

    //Get user
    //public static User getUser(){
    //return [SELECT Id, Username, Name FROM User WHERE Username Like 'projetos@werise.com.br' LIMIT 1];
    //}
    //
    ////Create leads
    //public static List<Lead> createLeads(Integer NumLeads, Id idOwner){
    //    
    //    List<Lead> CreatedLeads = new List<Lead>();
    //    for(Integer i=0; i<NumLeads; i++){
    //        
    //        Lead theLead = new Lead(
    //        LastName = 'Test Lead n:' + i,
    //        Company = 'São Paulo',
    //        Status = 'MQL',
    //        OwnerId = idOwner);
    //      
    //       CreatedLeads.add(theLead);
    //    }
    //  
    //    insert CreatedLeads;
    //    return CreatedLeads;
    //}
//
    //public static List<Account> createAccounts(Integer NumAccts){
    //  
    //    List<Account> CreatedAccounts = new List<Account>();
    //    for(Integer i=0; i<NumAccts; i++){
    //      
    //        Account theAccount = new Account(Name='TestAccount n:' + i);
    //        theAccount.BillingCity = 'São Paulo';
    //        theAccount.BillingCountry = 'Brazil' ;
    //        theAccount.BillingState = 'SP';
    //        theAccount.BillingStreet = 'Rua teste, 10';
    //        theAccount.Type = 'Base';
    //      
    //        CreatedAccounts.add(theAccount);
    //    }
    //  
    //    insert CreatedAccounts;
    //    return CreatedAccounts;
    //}
    //
    //public static List<Contact> createContacts(List<Account> Accounts, Integer NumberContactsPerAccount){
    //  
    //    List<Contact> CreatedContacts = new List<Contact>();
    //    for (Integer j=0; j<Accounts.size(); j++) {
    //        Account acct = Accounts[j];
    //        for (Integer k=NumberContactsPerAccount*j;
    //             k<NumberContactsPerAccount*(j+1); k++) {
    //                 CreatedContacts.add(new Contact(FirstName='Test-FirstName'+k,
    //                                                 LastName='Test-LastName'+k,
    //                                                 AccountId=acct.Id));
    //             }
    //    }
    //  
    //    insert CreatedContacts;
    //    return CreatedContacts;
    //}
    //   public static List<Opportunity> createOpps(List<Contact> CreatedContacts, Date closeDate, Campaign camp){
    //    
    //    List<Opportunity> createdOpps = new List<Opportunity>();
    //    for(Contact theContact : CreatedContacts)
    //    {
    //        Opportunity opp = new Opportunity(
    //            Name = 'Opp Teste -- ' + theContact.FirstName,
    //            Type = 'Novo Cliente',
    //            Amount = 10000,
    //            StageName = 'Proposta Apresentada',
    //            AccountId = theContact.AccountId,
    //            CloseDate = closeDate,
    //            LeadSource = 'E-book',
    //            CampaignId = camp.Id
    //        );
    //      
    //        createdOpps.add(opp);
    //    }
    //  
    //    insert createdOpps;
    //    return createdOpps;
    //}
    
    public static List<Product2> createProduct(List<String> listProdCode){
        
        List<Product2> listProducts = new List<Product2>();
        
        for(Integer i=0; i<listProdCode.size(); i++){
            
            Product2 theProd = new Product2(
                Name='Product Test ('+listProdCode[i]+')',
                IsActive = true,
                ProductCode = listProdCode[i]
            );
            
            listProducts.add(theProd);
        }
        
        insert listProducts;
        return listProducts;
    }
    
    public static List<PricebookEntry> createPricebookEntry(List<Product2> pProd)
    {
        Pricebook2 standardPricebook = new Pricebook2(
        	Id = Test.getStandardPricebookId(),
        	IsActive = true
		);
        update standardPricebook;

        List<PricebookEntry> entries = new List<PricebookEntry>();
        for(Product2 prod : pProd){
            PricebookEntry thePricebook = new PricebookEntry();
            //thePricebook.Pricebook2Id = Test.getStandardPricebookId();
            thePricebook.Pricebook2Id = standardPricebook.Id;
            thePricebook.Product2Id = prod.Id;
            thePricebook.IsActive = true;
            if(prod.ProductCode == '50'){
                thePricebook.UnitPrice = 300;
            }
            else if(prod.ProductCode == '51'){
                thePricebook.UnitPrice = 800;
            }
            else {
                thePricebook.UnitPrice = 11000;
            }
            entries.add(thePricebook);
        }
        insert entries;
        return entries;
    }

    
    //public static List<OpportunityLineItem> addProductToOpportunity(PricebookEntry productPricebookEntry, Product2 pProd, List<Opportunity> lpOpps)
    //{
    //    List<OpportunityLineItem> listOli = new List<OpportunityLineItem>();
        
    //    for(Opportunity opp:lpOpps)
    //    {
    //        OpportunityLineItem ol = new OpportunityLineItem();
    //        ol.OpportunityId = opp.Id;
    //        ol.Quantity = 1;
    //        ol.UnitPrice = 10000.00;
    //        ol.PricebookEntryId = productPricebookEntry.Id;
    //        ol.Product2Id = pProd.Id;
    //        listOli.add(ol);
    //    }
    //    insert listOli;
    //    return listOli;
    //}
    
    //public static List<Campaign> createCampaigns(Integer NumberCampaigns){
    //    List<Campaign> listCampaigns = new List<Campaign>();
        
    //    for(Integer i=0; i< NumberCampaigns; i++){
            
    //        Campaign campaign = new Campaign(
    //            Name = 'Campaign Test',
    //            IsActive = true
    //        );
            
    //        listCampaigns.add(campaign);
    //    }
        
    //    insert listCampaigns;
    //   return listCampaigns;
    //}
}