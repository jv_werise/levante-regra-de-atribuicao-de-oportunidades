/*******************************************************************************
*                               Werise - 2020
*-------------------------------------------------------------------------------
*
*	Class to update the SDRs and enqueue the accounts to process
*
*   Test class : AccountQueueDistribution_test
*   Called from : AccountTrigger
*
* NAME: AccountQueueDistribution
* AUTHOR: João Vitor Ramos                                      DATE: 16/10/2020
*******************************************************************************/
public class AccountQueueDistribution {

    public static void distributeAccount(List<Account> listAccountCriteria, Set<Id> setGroupIdToUpdate){
        //get all names of list and update the sdrs settings
        AccountQueueDistribution.verifySdrs(setGroupIdToUpdate);

        //delaying the call of changeAccountOwner to avoid race condintion
        String str = String.valueOf(Crypto.getRandomInteger()).substring(1);
        str = (str+'0');
        str = str.substring(str.lastIndexOf('0')-3);
        Integer nmb = Integer.ValueOf(str);
        while(nmb > 0){
            nmb--;
        }
        AccountQueueDistributionBatch.changeAccountOwner(setGroupIdToUpdate, listAccountCriteria);
    }
 
    public static void verifySdrs(Set<Id> setGroupIdToUpdate){
        System.debug('%%%%%%%%%%%%%%%% ACCOUNTQUEUEDISTRIBUTION UPDATING THE SDRS %%%%%%%%%%%%%%%%');
        List<Group> queueList = new List<Group>();

        if(!test.isRunningTest()){ queueList = [SELECT Id, Name FROM Group WHERE Id IN: setGroupIdToUpdate];
        } else { queueList = [SELECT Id, Name, Type FROM Group WHERE Name IN ('GrupoJunior', 'GrupoPleno', 'GrupoPlenoInfinite') AND Type = 'Regular'];
        }

        //for each list
        for(Group gp : queueList){
            List<GrupoSDRs__c> listSdrToDel = new List<GrupoSDRs__c>();
            List<GrupoSDRs__c> listSrdToAdd = new List<GrupoSDRs__c>();
            List<GrupoSDRs__c> listSdrToCreate = new List<GrupoSDRs__c>();
            List<GrupoSDRs__c> listSdrFiltered = new List<GrupoSDRs__c>();
            List<GrupoSDRs__c> listSdr = new List<GrupoSDRs__c>();
            Set<Id> setGroupMemberId = new Set<Id>();
            Map<Id, String> mapUserIdByName = new Map<Id, String>();
            Map<Id, String> mapSdrIdByName = new Map<Id, String>();

            //query the group member of the queue
            List<GroupMember> members = [SELECT Id, GroupId, UserOrGroupId FROM GroupMember WHERE GroupId =: gp.Id];
            for(GroupMember gm : members) setGroupMemberId.add(gm.UserOrGroupId);

            //query the users to get the names
            List<User> userList = [SELECT Id, Name, Alias FROM User WHERE Id IN: setGroupMemberId];
            for(User us : userList) mapUserIdByName.put(us.Id, us.Alias);

            //get all SDRs
            listSdr = GrupoSDRs__c.getall().values();
            //filter the sdrs for the specific queue
            if(!listSdr.isEmpty()){
                for(GrupoSDRs__c sdr : listSdr){
                    if(sdr.Id_Fila__c == gp.Id){
                        listSdrFiltered.add(sdr);
                        mapSdrIdByName.put(sdr.Id_Usuario__c, sdr.Name);
                    }
                }
            }
            //verify if no have SRD and create
            if(listSdrFiltered.isEmpty()){
                for(GroupMember groupMember : members){
                    listSdrToCreate.add( new GrupoSDRs__c(Id_Usuario__c = groupMember.UserOrGroupId,
                                                    Recebeu_Conta__c = false,
                                                    Id_Fila__c = gp.Id,
                                                    Name = mapUserIdByName.get(groupMember.UserOrGroupId) + '-' + String.valueOf(gp.Id).substring(11)));
                }
                if(!listSdrToCreate.isEmpty()) upsert listSdrToCreate;
            }

            //if have some SDR verify if existy in the list
            if(!listSdrFiltered.isEmpty()) {
                for(GrupoSDRs__c sdrs : listSdrFiltered){
                    //if not existy on the map of users delete the SDR
                    if(mapUserIdByName.get(sdrs.Id_Usuario__c) == null) listSdrToDel.add(sdrs);
                }
                if(!listSdrToDel.isEmpty()) delete listSdrToDel;
            }

            if(!listSdrFiltered.isEmpty()){
                for(User us : userList){
                    //if the user not existy on the map of SDR insert the SDR
                    if(mapSdrIdByName.get(us.Id) == null){
                        listSrdToAdd.add(new GrupoSDRs__c(Id_Usuario__c = us.Id,
                                                    Recebeu_Conta__c = false,
                                                    Id_Fila__c = gp.Id,
                                                    Name = us.Alias  + '-' + String.valueOf(gp.Id).substring(11)));
                    }
                }
            if(!listSrdToAdd.isEmpty()) upsert listSrdToAdd;
            }
        }
    }
}