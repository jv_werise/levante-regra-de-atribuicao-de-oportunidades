/*******************************************************************************
*                               Werise - 2020
*-------------------------------------------------------------------------------
*                              Trigger on Opportunnity 
*
* NAME: OpportunityTrigger
* AUTHOR: Jefferson F. Presotto                                DATE: 17/02/2020
* UPDATED: João Vitor Ramos                                    DATE: 16/02/2020
*******************************************************************************/
trigger OpportunityTrigger on Opportunity (after insert, before update, after update) {
    List<Opportunity> listCreateOppProd = new List<Opportunity>();
    List<Opportunity> listCreateOppProdUpd = new List<Opportunity>();
    //for insert
    if(Trigger.isInsert){
        if(Trigger.isAfter){
            for(Opportunity opp : Trigger.New){
                if(opp.Criado_Mkt_Cld__c == true ){
                    if(opp.OpportunityLineItems.size() == 0){
                        listCreateOppProd.add(opp);
                    }
                }
            }
            if(!listCreateOppProd.isEmpty()) ShoppingCartProcess.createProduct(listCreateOppProd);

            //calling the classes to automate the owner distribution of Opportunities and its Accounts
            List<Account> accountList = new List<Account>();
            List<Account> accountListFiltered = new List<Account>();
            Set<Id> setQueueIds = new Set<Id>();
            User usuario = [select Id, alias from User where alias = 'werise'];

            for(Group gp : [SELECT Id FROM Group WHERE Name IN ('GrupoJunior', 'GrupoPleno', 'GrupoPlenoInfinite') AND Type = 'Regular']){
                setQueueIds.add(gp.Id);
            }
            for(Opportunity opp : Trigger.New){
                if(opp.OwnerId == usuario.Id){
                Account acc = [SELECT Id, Name, OwnerId, CreatedDate, Owner.Alias  FROM Account WHERE Id = :opp.AccountId ];
                accountList.add(acc);
                }
            }
            for(Account acc : accountList){
                //converting the CreatedDate from datetime do date
                Date accDate = date.newinstance(acc.CreatedDate.year(), acc.CreatedDate.month(), acc.CreatedDate.day());
                if(accDate == Date.today() && acc.OwnerId == usuario.Id){
                    accountListFiltered.add(acc);
                }
            }
            if(!accountListFiltered.isEmpty()){
                AccountQueueDistribution.distributeAccount(accountListFiltered, setQueueIds);
            }
        }
    }
    //for update
    if(Trigger.isUpdate){
        if(Trigger.isBefore){
            for(Opportunity opp : Trigger.new){
                //Após ser fechado
                if(opp.Criado_Mkt_Cld__c == true 
                   && opp.StageName == 'Fechado e perdido'
                   && opp.Numero_Visitas_CheckOut__c != trigger.oldMap.get(opp.Id).Numero_Visitas_CheckOut__c){
                       opp.StageName = 'Classificação';
                       opp.CloseDate = System.today() +7;
                       opp.dataUltimoCheckout__c = System.Now();
                       CreateChatterNotification.createChatterNotification(opp.Id, opp.OwnerId);
                   }
                //Durante os 7 dias na fase Classificação
                if(opp.Criado_Mkt_Cld__c == true 
                   && opp.StageName == 'Classificação'
                   && opp.Numero_Visitas_CheckOut__c != trigger.oldMap.get(opp.Id).Numero_Visitas_CheckOut__c){
                       //opp.StageName = 'Classificação';
                       //opp.CloseDate = System.today() +7;
                       opp.dataUltimoCheckout__c = System.Now();
                       CreateChatterNotification.createChatterNotification(opp.Id, opp.OwnerId);
                   }
                if(opp.StageName == 'Fechado e ganho' && trigger.oldMap.get(opp.Id).StageName != 'Fechado e ganho'){
                    if(!opp.Name.contains(' - Fechado e ganho - ')){
                        opp.Name += ' - Fechado e ganho - ' + System.today().day() + '/' + System.today().month() + '/' + System.today().year();
                    }
                }
            }
        }
        if(Trigger.isAfter){
            for(Opportunity opp : Trigger.New){
                if(opp.Criado_Mkt_Cld__c == true && trigger.oldMap.get(opp.Id).Criado_Mkt_Cld__c != true){
                    if(opp.OpportunityLineItems.size() == 0){
                        listCreateOppProdUpd.add(opp);
                    }
                }
            }
            if(!listCreateOppProdUpd.isEmpty()) ShoppingCartProcess.createProduct(listCreateOppProdUpd);
        }
    }
}