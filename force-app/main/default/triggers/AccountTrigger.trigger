/*******************************************************************************
*                               Werise - 2020
*-------------------------------------------------------------------------------
*
*	Trigger on Account SObject for (Insert or Update) && (After)
*
*   Test Class :
*   Status: deactivated
*
* NAME: AccountTrigger
*AUTHOR: João Vitor Ramos                                      DATE: 14/10/2020
*******************************************************************************/

trigger AccountTrigger on Account (before insert, before update, after insert, after update) {

//    System.debug('%%%%%%%%%%%%%%%% ACCOUNT TRIGGER IS RUNNING %%%%%%%%%%%%%%%%');
//    Set<Id> setQueueIds = new Set<Id>();
//
//    //get the queue ids
//    for(Group gp : [SELECT Id, Name, type FROM Group WHERE Name != 'GrupoSDRs' AND Type = 'Regular']) setQueueIds.add(gp.Id);
//    User usuario = [select Id, alias from User where alias = 'werise'];
//    
//    //If trigger is insert
//    if(Trigger.isInsert){
//        //after insert
//        if(Trigger.isAfter){
//            System.debug('%%%%%%%%%%%%%%%% ACCOUNT TRIGGER IS RUNNING AFTER INSERT %%%%%%%%%%%%%%%%');
//            /**verifys if the account has queue owner and distribute the the queue users */
//            List<Account> listAccountCriteria = new List<Account>();
//            for(Account acc : Trigger.new){
//                //validate if the account has a queue as owner
//                //if(acc.OwnerId == usuario.Id && acc.RecordTypeId == rt.Id){
//                if(acc.OwnerId == usuario.Id){
//                    listAccountCriteria.add(acc);
//                }            
//            }
//            if(!listAccountCriteria.isEmpty()) AccountQueueDistribution.distributeAccount(listAccountCriteria, setQueueIds);
//        }
//    }
// 
//    //If trigger is update
//    else if(Trigger.isUpdate){
//        //after update
//        if(Trigger.isAfter){
//            System.debug('%%%%%%%%%%%%%%%% ACCOUNT TRIGGER IS RUNNING AFTER UPDATE %%%%%%%%%%%%%%%%');
//            /**verifys if the account has queue owner and distribute the the queue users */
//            List<Account> listAccountCriteria = new List<Account>();
//            Set<Id> setGroupIdToUpdate = new Set<Id>();
//            for(Account acc : Trigger.new){
//                //validate if the account has a queue as owner
//                if(acc.OwnerId == usuario.Id){ 
//                    listAccountCriteria.add(acc);
//                    //setGroupIdToUpdate.add(acc.OwnerId);
//                }
//            }
//            if(!listAccountCriteria.isEmpty()) AccountQueueDistribution.distributeAccount(listAccountCriteria, setQueueIds);
//        }
//    }
}